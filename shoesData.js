let shoesData = [
{
    id : 0,
    title : "White and Black",
    content : "Born in France",
    price : 120000,
    img: "https://codingapple1.github.io/shop/shoes1.jpg",
    quantity: 5,
},

{
    id : 1,
    title : "Red Knit",
    content : "Born in Seoul",
    price : 110000,
    img: "https://codingapple1.github.io/shop/shoes2.jpg",
    quantity: 2,
},

{
    id : 2,
    title : "Grey Yordan",
    content : "Born in the States",
    price : 130000,
    img: "https://codingapple1.github.io/shop/shoes3.jpg",
    quantity: 3,
},
{
    id : 3,
    title : "3White and Black",
    content : "3Born in France",
    price : 1120000,
    img: "https://codingapple1.github.io/shop/shoes4.jpg",
    quantity: 5,
},
{
    id : 4,
    title : "4Red Knit",
    content : "4Born in Seoul",
    price : 3110000,
    img: "https://codingapple1.github.io/shop/shoes5.jpg",
    quantity: 1,
},

{
    id : 5,
    title : "5Grey Yordan",
    content : "5Born in the States",
    price : 2130000,
    img: "https://codingapple1.github.io/shop/shoes6.jpg",
    quantity: 1,
}
] 


export default shoesData;